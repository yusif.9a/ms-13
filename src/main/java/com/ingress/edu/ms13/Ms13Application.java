package com.ingress.edu.ms13;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms13Application {

	public static void main(String[] args) {
		SpringApplication.run(Ms13Application.class, args);
	}

}
